package com.abdulrhman.springboot.api.security;

import com.abdulrhman.springboot.api.todos.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

public class UserService implements UserDetailsService {


    @Autowired
    UserRepository userRepository;

    private PasswordEncoder passwordEncoder(){
            return new BCryptPasswordEncoder();
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return new User("abc",passwordEncoder().encode("password"), AuthorityUtils.NO_AUTHORITIES);
    }

    public void save(AppUser user) {
        user.setPassword(passwordEncoder().encode(user.getPassword()));
        this.userRepository.save(user);
    }


        public List<AppUser> findAll() {
            return userRepository.findAll();
        }

}
