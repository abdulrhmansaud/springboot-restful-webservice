package com.abdulrhman.springboot.api.todos;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/todo")
public class TodoController {

    @Autowired
    private TodoService todoService;


    @GetMapping(value = { "", "/" })
    public ResponseEntity<List<Todo>> listTodo() {

        List<Todo> result = todoService.findall();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = { "/{id}" })
    public ResponseEntity<Todo> GetTodobyId(@PathVariable String id) {

        Todo result = todoService.getById(id);
        return new ResponseEntity<>(result, HttpStatus.OK);

    }
    @PostMapping(value = { "", "/" })
    public ResponseEntity<Todo> CreateNewTodo(@RequestBody Todo todo) {

        Todo result = todoService.save(todo);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @PutMapping(value = { "/{id}" })
    public Todo update(@PathVariable String id, @RequestBody Todo todo) {
        return todoService.update(id, todo);
    }
    @DeleteMapping(value = { "/{id}" })
    public ResponseEntity<Void> DeleteTodo(@PathVariable String id) {
        todoService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
