package com.abdulrhman.springboot.api.todos;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import sun.plugin2.message.Message;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Document(collection = "todo")
public class Todo {

    @Id
    private  String id;

    @NotNull(message="Title is Required ")
    @Size( min=3,message = "size most be at least 3 characers long")
    private  String title;

    @NotNull(message = "Descraption is required")
    private  String Descraption;
    private  String Author;
    private long timestamp;


    public Todo() {

        this.timestamp = System.currentTimeMillis();
    }


    public Todo(String id, String title, String descraption, String author) {
        this.id = id;
        this.title = title;
        this.Descraption = descraption;
        this.Author = author;
        this.timestamp = System.currentTimeMillis();
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescraption() {
        return Descraption;
    }

    public String getAuthor() {
        return Author;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTilte(String title) {
        this.title = title;
    }

    public void setDescraption(String descraption) {
        Descraption = descraption;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
