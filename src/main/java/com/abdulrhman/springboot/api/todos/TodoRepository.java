package com.abdulrhman.springboot.api.todos;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface TodoRepository extends MongoRepository<Todo,String> {

    Todo findBytitle(String title);
}
